extern crate failure;
extern crate serde;
extern crate serde_json;
extern crate structopt;
#[macro_use]
extern crate structopt_derive;

use std::collections::HashMap;
use structopt::StructOpt;
use std::fs::OpenOptions;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Write;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(StructOpt, Debug)]
#[structopt(name = "Tradoc", about = "An asciidoc translation extractor")]
enum Action {
    #[structopt(name = "import")]
    Import {
        #[structopt(help = "The input asciidoc file")] adocfile: String,

        #[structopt(help = "The JSON output file")] jsonfile: String,
    },

    #[structopt(name = "export")]
    Export {
        #[structopt(help = "The template asciidoc file")]
        templatefile: String,

        #[structopt(help = "The JSON input file")] jsonfile: String,

        #[structopt(help = "The output asciidoc file")] adocfile: String,
    },
}

fn iterate_adocfile<B, F>(adocfile: B, mut func: F) -> Result<()>
where
    B: BufRead,
    F: FnMut(&str, usize) -> Result<()>,
{
    let mut paragraph = "".to_string();
    let mut i = 0;
    for line in adocfile.lines() {
        let line = line?;
        if line == "" {
            if paragraph != "" {
                func(&paragraph, i)?;
                i += 1;
            }
            paragraph = "".to_string();
        } else {
            if paragraph == "" {
                paragraph = line;
            } else {
                paragraph = format!("{}\n{}", paragraph, line);
            }
        }
    }
    if paragraph != "" {
        func(&paragraph, i)?;
    }
    Ok(())
}

fn import(adocfile: &str, jsonfile: &str) -> Result<()> {
    let adocfile = OpenOptions::new().read(true).open(adocfile)?;

    let mut map: HashMap<String, String> =
        match OpenOptions::new().read(true).open(jsonfile) {
            Ok(f) => serde_json::from_reader(f)?,
            Err(_) => HashMap::new(),
        };

    let jsonfile = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open(jsonfile)?;

    let adocfile = BufReader::new(adocfile);

    iterate_adocfile(adocfile, |s, _| {
        map.entry(s.to_string()).or_insert("".to_string());
        Ok(())
    })?;

    serde_json::to_writer(jsonfile, &map)?;

    Ok(())
}

trait NonEmptyOr {
    fn nonempty_or(&self, default: &Self) -> Self;
}

impl NonEmptyOr for String {
    fn nonempty_or(&self, default: &Self) -> Self {
        if self.is_empty() {
            default.clone()
        } else {
            self.clone()
        }
    }
}

fn export(templatefile: &str, jsonfile: &str, adocfile: &str) -> Result<()> {
    let templatefile = OpenOptions::new().read(true).open(templatefile)?;
    let jsonfile = OpenOptions::new().read(true).open(jsonfile)?;
    let mut adocfile = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open(adocfile)?;

    let templatefile = BufReader::new(templatefile);

    let map: HashMap<String, String> = serde_json::from_reader(jsonfile)?;

    iterate_adocfile(templatefile, |s, i| {
        if i != 0 {
            adocfile.write("\n".as_bytes())?;
        }

        let paragraph: String = map.get(s)
            .unwrap_or(&s.to_string())
            .to_string()
            .nonempty_or(&s.to_string());

        adocfile.write(format!("{}\n", paragraph).as_bytes())?;

        Ok(())
    })?;

    Ok(())
}

fn main() {
    match Action::from_args() {
        Action::Import { adocfile, jsonfile } => import(&adocfile, &jsonfile),
        Action::Export {
            templatefile,
            jsonfile,
            adocfile,
        } => export(&templatefile, &jsonfile, &adocfile),
    }.expect("Something went wrong");
}
